package com.jameel.root.magento;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;

import org.apache.http.entity.StringEntity;
import org.apache.http.util.EncodingUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by root on 18/11/15.
 */
public class PayUWebView extends Activity {

    WebView webView;

    PayUFormA payUFormA;
    String postData,str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payu_webview);
        payUFormA = new PayUFormA("fd3e847he",(float) 10.0, "T-Shirt",
                "AAA", "jameel@codilar.com",
                "910123456789", "http://codilar.net/ack/checkout/onepage/success", "http://yahoo.com");


        str = payUFormA.key+"|"+payUFormA.txnid+"|"+payUFormA.amount+"|"+payUFormA.productinfo+"|"+payUFormA.firstname+"|"+payUFormA.email+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+payUFormA.SALT;
        async_post(str);

        webView = (WebView) findViewById(R.id.resp);
        webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportZoom(true);       //Zoom Control on web (You don't need this
        //if ROM supports Multi-Touch
        webView.getSettings().setBuiltInZoomControls(true);
        //webView.postUrl("https://test.payu.in/_payment", EncodingUtils.getBytes(postData, "BASE64"));
        //Log.d("POST : ",postData);

    }


    public void async_post(String str) {

        //do a twiiter search with a http post

        String url = "http://192.168.1.25/demo/hash.php";
        new HttpTask(str).execute(url);


    }


    private class HttpTask extends AsyncTask<String, Void, String> {

        String str;

        public HttpTask(String str) {
            this.str = str;
        }

        @Override
        protected String doInBackground(String... params) {

            URL url = null;
            try {
                url = new URL(params[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Map<String, String> dataMap = new HashMap<String, String>();
            dataMap.put("hash", str);

            StringBuilder postBody = new StringBuilder();
            Iterator iterator = dataMap.entrySet().iterator();

            while (iterator.hasNext()) {
                Map.Entry param = (Map.Entry) iterator.next();
                postBody.append(param.getKey()).append('=')
                        .append(param.getValue());
                if (iterator.hasNext()) {
                    postBody.append('&');
                }
            }
            String body = postBody.toString();
            byte[] bytes = body.getBytes();

            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setFixedLengthStreamingMode(bytes.length);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded;charset=UTF-8");

                OutputStream out = conn.getOutputStream();
                out.write(bytes);
                out.close();

                int status = conn.getResponseCode();
                Log.d("Status code : ", "" + status + "\n" + conn.getResponseMessage());

                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                StringBuilder sb = new StringBuilder();
                String output;
                while ((output = br.readLine()) != null) {
                    sb.append(output);
                    Log.d("Body : ", sb.toString());
                }
                return sb.toString();
            }
            catch (Exception e) {

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("Body : ", s);
            try {
                JSONObject js = new JSONObject(s);

            postData = "email=jameel@codilar.com" +
                    "&firstname=AAA" +
                    "&furl=http://yahoo.com" +
                    "&hash="+js.getString("hash") +
                    "&key=gtKFFx" +
                    "&phone=910123456789" +
                    "&productinfo=T-Shirt" +
                    "&surl=http://codilar.net/ack/checkout/onepage/success" +
                    "&txnid=fd3e847he" +
                    "&amount=10.0";
                webView.setWebViewClient(new myWebClient());
                webView.postUrl("https://test.payu.in/_payment", EncodingUtils.getBytes(postData, "BASE64"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }



    public class myWebClient extends WebViewClient {
        public void onPageStarted(WebView view, String url, Bitmap favicon){
            super.onPageStarted(view, url, favicon);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url){
            view.loadUrl(url);
            return true;
        }
    }


}
