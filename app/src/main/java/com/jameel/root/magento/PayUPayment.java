package com.jameel.root.magento;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by root on 18/11/15.
 */
public class PayUPayment extends Activity {
    

    PayUFormA mPayUFormA;
    Button test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payu_payment);

        test = (Button) findViewById(R.id.test);
        mPayUFormA = new PayUFormA("fd3e847h2",(float) 10.00, "T-Shirt",
                "AAA", "jameel@codilar.com",
                "910123456789", "http://google.com", "http://yahoo.com");

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new HttpTask(mPayUFormA).execute("https://test.payu.in/_payment");
            }
        });




    }


    private class HttpTask extends AsyncTask<String ,Void, String> {

        PayUFormA payUFormA;
        public HttpTask(PayUFormA payUFormA) {
            this.payUFormA = payUFormA;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                HttpClient client = new DefaultHttpClient();
                client.getParams().setParameter("http.protocol.content-charset", "UTF-8");
                URI uri = null;
                try {
                    uri = new URI(params[0]);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                HttpPost post = new HttpPost(uri);
                StringEntity postEntity = new StringEntity(new Gson().toJson(payUFormA), "UTF-8");
                post.setEntity(postEntity);
                post.addHeader("Content-Type", "application/json");
                post.addHeader("Accept", "application/json");


                HttpResponse httpResponse = null;
                try {
                    HttpHost target = new HttpHost(uri.getHost(), -1, uri.getScheme());
                    httpResponse = client.execute(target, post);
                    Log.d("Connection status : ", "" + httpResponse.getStatusLine() + "   ");
                    InputStream inputStraem = httpResponse.getEntity().getContent();
                    //String output = String.valueOf(httpResponse.getEntity().getContent());

                    StringWriter writer = new StringWriter();
                    IOUtils.copy(inputStraem, writer, "UTF-8");
                    String output = writer.toString();
                    Log.d("json",httpResponse.getStatusLine()+"\n"+ output);
                    return "Status code : "+httpResponse.getStatusLine()+"\n"+output;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Intent intent = new Intent(PayUPayment.this, PayUWebView.class);
            intent.putExtra("response",s);
            startActivity(intent);

        }
    }


}
