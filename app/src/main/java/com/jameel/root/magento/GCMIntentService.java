package com.jameel.root.magento;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GCMIntentService extends IntentService {

   public static final int NOTIFICATION_ID = 1000;
   NotificationManager mNotificationManager;
   NotificationCompat.Builder builder;
    private Bitmap message_bitmap;
    AQuery aq;

    public GCMIntentService() {
      super(GCMIntentService.class.getName());
   }

   @Override
   protected void onHandleIntent(Intent intent) {
      Bundle extras = intent.getExtras();

      if (!extras.isEmpty()) {

         // read extras as sent from server
         String message = extras.getString("message");
         String serverTime = extras.getString("timestamp");
         String url = extras.getString("image_url");
          aq = new AQuery(this);
          Log.d("Notification data : ", message + "\n" + serverTime + "\n" + url);
          aq.image(url, true, true, 50, 30, new BitmapAjaxCallback(){

              @Override
              public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status){

                  message_bitmap = bm;


              }

          });


         sendNotification("Message: " + message + "\n" + "Server Time: "
               + serverTime);
      }
      // Release the wake lock provided by the WakefulBroadcastReceiver.
      GCMBroadcastReceiver.completeWakefulIntent(intent);
   }


   private void sendNotification(String msg) {
      mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

      PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
              new Intent(this, MainActivity.class), 0);


       NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
            this).setSmallIcon(R.drawable.ic_launcher)
            .setContentTitle("Offers")
            .setContentText(msg)
            .setStyle(new NotificationCompat.BigPictureStyle()
                    .bigPicture(message_bitmap));

      mBuilder.setContentIntent(contentIntent);
      mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
   }

}