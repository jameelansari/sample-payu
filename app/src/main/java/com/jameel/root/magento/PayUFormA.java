package com.jameel.root.magento;

import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by root on 18/11/15.
 */
public class PayUFormA {

    MessageDigest md;

    public String txnid, productinfo, firstname, email, phone, surl, furl, hash;
    public final static String SALT = "eCwWELxi";
    public  String key = "gtKFFx";
    public float amount;

    public PayUFormA(String txnid, float amount, String productinfo,
                     String firstname, String email,
                     String phone, String surl, String furl) {
        this.txnid = txnid;
        this.amount = amount;
        this.productinfo = productinfo;
        this.firstname = firstname;
        this.email = email;
        this.phone = phone;
        this.surl = surl;
        this.furl = furl;
        hash = generateHash();

    }

    public  String generateHash() {

        String str = key+"|"+txnid+"|"+amount+"|"+productinfo+"|"+firstname+"|"+email+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+"|"+SALT;
        File file;
    try {
        file = new File(Environment.getExternalStorageDirectory() + File.separator + "loging.log");

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
            Log.d("Create : ","True");
        }
        else {
            Log.d("Create : ","false");
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(str);
        bw.close();
    } catch (Exception e) {
        Log.d("file : ","Not created");
    }
        StringBuffer hexString = new StringBuffer();
        try {

            MessageDigest md = MessageDigest.getInstance("SHA-512");
            FileInputStream fis = new FileInputStream(Environment.getExternalStorageDirectory() + File.separator + "loging.log");
            byte[] dataBytes = new byte[1024];
            int nread = 0;
            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }
            byte[] mdbytes = md.digest();
            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            System.out.println("Hex format : " + sb.toString());
            //convert the byte to hex format method 2

            for (int i = 0; i < mdbytes.length; i++) {
                hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
            }
            Log.d("Hash : ",""+hexString.toString());


        } catch (Exception e) {

        } finally {
            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "loging.log");
            Log.d("Delete file : ", "" + f.delete());
        }
        return hexString.toString();
    }

}
